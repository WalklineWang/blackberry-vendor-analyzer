VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIniFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const BufferSize = 1024

Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Private m_IniFileName As String

Public Sub SpecifyIniFile(FilePathName As String)

    m_IniFileName = FilePathName

End Sub

Private Function NoIniFile() As Boolean

    NoIniFile = True

    If m_IniFileName = vbNullString Then Exit Function

    NoIniFile = False

End Function

Public Function WriteString(Section As String, Key As String, Value As String) As Boolean

    WriteString = False

    If NoIniFile() Then Exit Function

    If WritePrivateProfileString(Section, Key, Value, m_IniFileName) = 0 Then Exit Function

    WriteString = True

End Function

Public Function ReadString(Section As String, Key As String, Optional Default As String = vbNullString) As String

    Dim ReturnStr As String
    Dim ReturnLng As Long

    ReadString = vbNullString

    If NoIniFile() Then Exit Function

    ReturnStr = Space(BufferSize)
    ReturnLng = GetPrivateProfileString(Section, Key, Default, ReturnStr, BufferSize, m_IniFileName)
    ReadString = StrConv(LeftB(StrConv(ReturnStr, vbFromUnicode), ReturnLng), vbUnicode)
    'Left(ReturnStr, ReturnLng)

End Function

Public Function ReadInteger(Section As String, Key As String, Optional Default As Integer = 0) As Integer

    ReadInteger = 0

    If NoIniFile() Then Exit Function

    ReadInteger = GetPrivateProfileInt(Section, Key, Default, m_IniFileName)

End Function

Private Sub Class_Initialize()

    m_IniFileName = vbNullString

End Sub
