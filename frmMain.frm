VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BlackBerry Vendor Analyzer"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5310
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   5310
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save and Refresh"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   590
      Left            =   5400
      TabIndex        =   15
      Top             =   7045
      Width           =   3375
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add"
      Default         =   -1  'True
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   7920
      TabIndex        =   13
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtInChinese 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   12
      Top             =   600
      Width           =   2415
   End
   Begin VB.TextBox txtInEnglish 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   11
      Top             =   120
      Width           =   2415
   End
   Begin VB.ListBox lstTranslate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5940
      Left            =   5400
      TabIndex        =   14
      Top             =   1035
      Width           =   3375
   End
   Begin VB.CommandButton cmdOption 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4680
      TabIndex        =   10
      Top             =   6315
      Width           =   495
   End
   Begin VB.CommandButton cmdCopyVendorID 
      Caption         =   "Copy"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   9
      Top             =   7260
      Width           =   975
   End
   Begin VB.TextBox txtVendorID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   7260
      Width           =   3975
   End
   Begin VB.CommandButton cmdCopyVendorName 
      Caption         =   "Copy"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   7
      Top             =   6780
      Width           =   975
   End
   Begin VB.TextBox txtVendorName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   6780
      Width           =   3975
   End
   Begin VB.ListBox lstVendorName 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5310
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   5055
   End
   Begin VB.CommandButton cmdFilename 
      Caption         =   "&Open"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox txtFilename 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   150
      Width           =   2895
   End
   Begin VendorAnalyzer.uc3DLine uc3DLine2 
      Height          =   30
      Left            =   120
      Top             =   600
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   53
   End
   Begin VB.Label lblVendorCounter 
      AutoSize        =   -1  'True
      Caption         =   "Total:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   240
      TabIndex        =   5
      Top             =   6330
      Width           =   840
   End
   Begin VB.Label lblVendorName 
      AutoSize        =   -1  'True
      Caption         =   "Vendor List:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   640
      Width           =   1260
   End
   Begin VB.Label lblFilename 
      AutoSize        =   -1  'True
      Caption         =   "&Filename:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   180
      Width           =   855
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim oDialog As New clsCommonDialog
Dim colVendorName As New Collection, colVendorID As New Collection
Dim colVendorInEnglish As New Collection, colVendorInChinese As New Collection
Dim AppPath As String
Dim isTranslateModified As Boolean
Dim WithEvents TT As clsTooltip
Attribute TT.VB_VarHelpID = -1

Private Sub doAnalysis()

    Dim document As New DOMDocument
    Dim domNode As IXMLDOMNode
    Dim vendorNode As IXMLDOMNode
    Dim vendorName As String, vendorID As String

    lstVendorName.Clear
    txtVendorName.Text = ""
    lblVendorCounter = ""
    
    document.Load txtFilename.Text
    If document.documentElement Is Nothing Then
        MsgBox "Vendor.xml content error!", vbExclamation, "Error"
        
        Exit Sub
    End If
    
    Set domNode = document.selectSingleNode("vendorsrc")
    lblVendorName = "Vendor List (" & domNode.Attributes.getNamedItem("version").Text & "):"

    For Each vendorNode In domNode.childNodes
        vendorID = vendorNode.Attributes.getNamedItem("id").Text
        vendorID = Val("&H" & Replace(vendorID, "0x", "", , , vbTextCompare))
        vendorName = vendorNode.Attributes.getNamedItem("Name").Text
        
        colVendorName.Add vendorName
        colVendorID.Add vendorID
        
        If (Len(vendorID) = 1) Then vendorID = "  " & vendorID
        If (Len(vendorID) = 2) Then vendorID = " " & vendorID
        
        lstVendorName.AddItem vendorID & " " & vendorName
    Next
    
    Call exportData
    lblVendorCounter = "Total: " & lstVendorName.ListCount & " item(s)"
    
    Set vendorNode = Nothing
    Set domNode = Nothing
    Set document = Nothing
    
End Sub

Private Sub exportData()

    Dim vendorName As String, vendorID As String, tempName As String, tempID As String, keyword As String, lastString As String
    Dim iCount As Integer, jCount As Integer, pos As Integer

    txtVendorName.Text = ""
    txtVendorID.Text = ""

    For iCount = 1 To colVendorName.Count
        vendorName = colVendorName.Item(iCount)
        vendorID = colVendorID.Item(iCount)
        
        For jCount = 1 To colVendorInEnglish.Count
            keyword = colVendorInEnglish.Item(jCount)
            pos = InStr(1, vendorName, keyword, vbTextCompare)

            If pos > 0 Then
                lastString = Mid(vendorName, pos + Len(colVendorInEnglish.Item(jCount)), 1)
                
                If lastString = "" Or lastString = "/" Then
                    vendorName = Replace(vendorName, colVendorInEnglish.Item(jCount), colVendorInEnglish.Item(jCount) & colVendorInChinese.Item(jCount))
                End If
            End If
        Next
        
        tempName = tempName & """" & vendorName & """, "
        tempID = tempID & """" & vendorID & """, "
    Next

    tempName = Replace(tempName, "RIMWebSLZero", "RIM WebSL Zero (EDT工程机)", , , vbTextCompare)
    tempName = Replace(tempName, "RIM Engineering Prototypes", "RIM Engineering Prototypes (DVT测试机)", , , vbTextCompare)
    
    tempName = tempName & """" & "RIM WLAN" & """, "
    tempID = tempID & """" & "163" & """, "
    tempName = tempName & """" & "非正常销售、非正常工作状态手机" & """, "
    tempID = tempID & """" & "-1" & """, "
    
    
    
    tempName = Left(tempName, Len(tempName) - 2)
    tempID = Left(tempID, Len(tempID) - 2)
    
    txtVendorName.Text = "{" & tempName & "}"
    txtVendorID.Text = "{" & tempID & "}"

End Sub

Private Sub cmdAdd_Click()

    colVendorInEnglish.Add txtInEnglish.Text
    colVendorInChinese.Add txtInChinese.Text

    lstTranslate.AddItem txtInEnglish.Text & " - " & txtInChinese.Text
    
    txtInEnglish.Text = ""
    txtInChinese.Text = ""
    
    isTranslateModified = True
    cmdSave.Enabled = True

End Sub

Private Sub cmdCopyVendorID_Click()

    Call ClipboardSetText(Me.hwnd, txtVendorID.Text)

End Sub

Private Sub cmdCopyVendorName_Click()

    Call ClipboardSetText(Me.hwnd, txtVendorName.Text)

End Sub

Private Sub cmdFilename_Click()

    Dim strFilename As String
    Dim initDir As String
    
    initDir = GetSpecialfolder(CSIDL_PROGRAM_FILES_COMMON)
    If initDir <> "" Then initDir = initDir & "\Research In Motion\AppLoader"
    
    If oDialog.VBGetOpenFileName(strFilename, , True, False, True, True, "Vendor.xml|vendor.xml|XML Files (*.xml)|*.xml", , initDir, "Open File") Then
        txtFilename.Text = strFilename
        
        If Len(Trim(txtFilename.Text)) > 0 Then
            txtFilename.SelStart = Len(txtFilename.Text)
            txtFilename.SetFocus
            
            Call doAnalysis
        End If
    End If
    
End Sub

Private Sub cmdOption_Click()

    If cmdOption.Caption = ">>" Then
        cmdOption.Caption = "<<"
        Me.Width = 9000
    Else
        cmdOption.Caption = ">>"
        Me.Width = 5400
    End If

End Sub

Private Sub cmdSave_Click()

    Dim document As New DOMDocument
    Dim vendorsrc As IXMLDOMElement, vendor As IXMLDOMElement
    Dim pi As IXMLDOMProcessingInstruction
    Dim vendorNameInEnglish As String, vendorNameInChinese As String, tempName As String, tempID As String
    Dim xmlFilename As String
    Dim iCount As Integer
    
    If Not isTranslateModified Then Exit Sub

    xmlFilename = AppPath & App.EXEName & ".xml"
    
    Set vendorsrc = document.createElement("vendorsrc")
    For iCount = 1 To colVendorInEnglish.Count
        Set vendor = document.createElement("vendor")
        Call vendor.setAttribute("NameInEnglish", colVendorInEnglish.Item(iCount))
        Call vendor.setAttribute("NameInChinese", colVendorInChinese.Item(iCount))
        vendorsrc.appendChild vendor
    Next
    document.appendChild vendorsrc
    
    Set pi = document.createProcessingInstruction("xml", "version='1.0' encoding='gb2312'")
    Call document.insertBefore(pi, document.childNodes(0))
    Call document.save(xmlFilename)
    
    Set pi = Nothing
    Set vendorsrc = Nothing
    Set vendor = Nothing
    Set document = Nothing
    
    MsgBox "Settings saved!", , "Tips"
    cmdSave.Enabled = False
    
    Call exportData

End Sub

Private Sub Form_Initialize()

    'If App.PrevInstance Then End

End Sub

Private Sub Form_Load()

    Dim document As New DOMDocument
    Dim domNode As IXMLDOMNode
    Dim vendorNode As IXMLDOMNode
    Dim vendorNameInEnglish As String, vendorNameInChinese As String, tempName As String, tempID As String
    
    frmMain.Caption = App.Title & " - [" & App.Major & "." & App.Minor & "." & App.Revision & "]"
    AppPath = IIf(Len(App.Path) > 3, App.Path & "\", App.Path)
    isTranslateModified = False
    
    Set TT = New clsTooltip
    TT.AddTool lstVendorName, , vbWhite, vbBlack, , , , , 5

    document.Load AppPath & App.EXEName & ".xml"
    If document.documentElement Is Nothing Then Exit Sub
    
    Set domNode = document.selectSingleNode("vendorsrc")

    For Each vendorNode In domNode.childNodes
        vendorNameInEnglish = vendorNode.Attributes.getNamedItem("NameInEnglish").Text
        colVendorInEnglish.Add vendorNameInEnglish

        vendorNameInChinese = vendorNode.Attributes.getNamedItem("NameInChinese").Text
        colVendorInChinese.Add vendorNameInChinese
        
        lstTranslate.AddItem vendorNameInEnglish & " - " & vendorNameInChinese
    Next
    
    Set vendorNode = Nothing
    Set domNode = Nothing
    Set document = Nothing
    
End Sub

Private Sub lstTranslate_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    Dim lParam As Long
    Dim lResult As Long
    Dim nIndex As Integer
    
    lParam = (CInt(y / Screen.TwipsPerPixelY) * 2 ^ 16) + CInt(x / Screen.TwipsPerPixelX)
    lResult = SendMessage(lstTranslate.hwnd, LB_ITEMFROMPOINT, 0, ByVal lParam)
  
    If lResult < 0 Or lResult > 32767 Then
        lstTranslate.TOOLTIPTEXT = ""
        Exit Sub
    End If
  
    nIndex = CInt(lResult)
    lstTranslate.TOOLTIPTEXT = lstTranslate.List(nIndex)

End Sub

Private Sub lstVendorName_DblClick()

    If cmdOption.Caption = ">>" Then Exit Sub

    txtInEnglish.Text = colVendorName.Item(lstVendorName.ListIndex + 1)
    txtInChinese.Text = "()"
    txtInChinese.SelStart = 1
    txtInChinese.SetFocus
    
End Sub

Private Sub lstVendorName_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    Dim lParam As Long
    Dim lResult As Long
    Dim nIndex As Integer, iCount As Integer
    Dim tooltips() As String, tooltip As String
    
    lParam = (CInt(y / Screen.TwipsPerPixelY) * 2 ^ 16) + CInt(x / Screen.TwipsPerPixelX)
    lResult = SendMessage(lstVendorName.hwnd, LB_ITEMFROMPOINT, 0, ByVal lParam)
  
    If lResult < 0 Or lResult > 32767 Then
        lstVendorName.TOOLTIPTEXT = ""
        Exit Sub
    End If
  
    nIndex = CInt(lResult) + 1
    tooltips = Split(colVendorName.Item(nIndex), "/", , vbTextCompare)
    
    tooltip = "ID: " & colVendorID.Item(nIndex) & vbCrLf & vbCrLf
    If UBound(tooltips) >= 0 Then
        For iCount = 0 To UBound(tooltips) - 1
            tooltip = tooltip & tooltips(iCount) & vbCrLf
        Next
        
        tooltip = tooltip & tooltips(iCount)
        TT.UpdateTool lstVendorName, tooltip
    End If

End Sub

Private Sub txtInChinese_Change()

    cmdAdd.Enabled = txtInEnglish.Text <> "" And txtInChinese.Text <> "()"
    
End Sub

Private Sub txtInEnglish_Change()

    cmdAdd.Enabled = txtInEnglish.Text <> "" And txtInChinese.Text <> "()"

End Sub

Private Sub txtVendorID_Change()

    cmdCopyVendorID.Enabled = txtVendorID.Text <> ""

End Sub

Private Sub txtVendorName_Change()

    cmdCopyVendorName.Enabled = txtVendorName.Text <> ""
    
End Sub
