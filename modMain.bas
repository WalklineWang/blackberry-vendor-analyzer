Attribute VB_Name = "modMain"
Option Explicit

Public Enum CSIDL_VALUES
    CSIDL_PROGRAM_FILES_COMMON = &H2B
    CSIDL_PROGRAM_FILES_COMMONX86 = &H2C
End Enum

Private Type SHITEMID
    cb As Long
    abID As Byte
End Type

Private Type ITEMIDLIST
    mkid As SHITEMID
End Type

Private Const NOERROR = 0
Public Const LB_ITEMFROMPOINT = &H1A9

Private Const CF_TEXT = 1
Private Const CF_UNICODETEXT = 13
Private Const GHND = &H42&

Private Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hwndOwner As Long, ByVal nFolder As Long, pidl As ITEMIDLIST) As Long
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal pszPath As String) As Long
Private Declare Function OpenClipboard Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function SetClipboardData Lib "user32" (ByVal Format As Long, ByVal hMem As Long) As Long
Private Declare Function CloseClipboard Lib "user32" () As Long
Private Declare Function EmptyClipboard Lib "user32" () As Long
Private Declare Function GlobalAlloc Lib "kernel32" (ByVal Flags As Long, ByVal length As Long) As Long
Private Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal pDest As Long, ByVal pSource As Long, ByVal length As Long)
Private Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Public Function FileExists(Filename As String) As Boolean

    Dim f As Long

    On Error Resume Next

    f = FreeFile
    Open Filename For Input As #f
    Close #f

    FileExists = Not (Err <> 0)

End Function

Public Function GetSpecialfolder(CSIDL As CSIDL_VALUES) As String

    Dim r As Long
    Dim IDL As ITEMIDLIST
    Dim Path As String
    
    r = SHGetSpecialFolderLocation(100, CSIDL, IDL)
 
    If r = NOERROR Then
        Path = Space(512)
        r = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path)
        GetSpecialfolder = Left(Path, InStr(Path, Chr(0)) - 1)
        
        Exit Function
    End If
    
    GetSpecialfolder = ""
    
End Function

Public Function ClipboardSetText(ByVal hWnd As Long, ByVal sText) As Boolean

    Dim hMem As Long, pMem As Long
    Dim s As String
    Dim bOk As Boolean
    
    If OpenClipboard(hWnd) = 0 Then Exit Function
    EmptyClipboard
    
    ' ANSI
    s = StrConv(sText, vbFromUnicode)
    hMem = GlobalAlloc(GHND, LenB(s) + 1)
    pMem = GlobalLock(hMem)
    CopyMemory ByVal pMem, ByVal StrPtr(s), LenB(s)
    GlobalUnlock hMem
    bOk = SetClipboardData(CF_TEXT, hMem) <> 0
    If Not bOk Then GlobalFree hMem
    
    ' UNICODE:
    hMem = GlobalAlloc(GHND, LenB(sText) + 2)
    pMem = GlobalLock(hMem)
    CopyMemory ByVal pMem, ByVal StrPtr(sText), LenB(sText)
    GlobalUnlock hMem
    bOk = SetClipboardData(CF_UNICODETEXT, hMem) <> 0
    If Not bOk Then GlobalFree hMem
    
    CloseClipboard
    ClipboardSetText = bOk
    
End Function
