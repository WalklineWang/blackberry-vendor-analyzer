VERSION 5.00
Begin VB.UserControl uc3DLine 
   CanGetFocus     =   0   'False
   ClientHeight    =   750
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4455
   HasDC           =   0   'False
   ScaleHeight     =   50
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   297
   ToolboxBitmap   =   "uc3DLine.ctx":0000
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      X1              =   48
      X2              =   248
      Y1              =   32
      Y2              =   32
   End
   Begin VB.Line Line1 
      BorderColor     =   &H0099A8AC&
      X1              =   48
      X2              =   248
      Y1              =   30
      Y2              =   30
   End
End
Attribute VB_Name = "uc3DLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum Style
    Horizontal = 0
    Vertical = 1
End Enum

Dim m_Style As Style

Public Property Get LineColor() As OLE_COLOR

    LineColor = Line1.BorderColor

End Property

Public Property Let LineColor(m_NewValue As OLE_COLOR)

    Line1.BorderColor = m_NewValue

    PropertyChanged "LineColor"

End Property

Public Property Get ShadowColor() As OLE_COLOR

    ShadowColor = Line2.BorderColor

End Property

Public Property Let ShadowColor(m_NewValue As OLE_COLOR)

    Line2.BorderColor = m_NewValue

    PropertyChanged "ShadowColor"

End Property

Public Property Get Style() As Style

    Style = m_Style

End Property

Public Property Let Style(m_NewValue As Style)

    If m_NewValue = m_Style Then Exit Property

    m_Style = m_NewValue
    If m_Style = Horizontal Then
        UserControl.Width = UserControl.Height
    Else
        UserControl.Height = UserControl.Width
    End If

    PropertyChanged "Style"

End Property

Private Sub UserControl_InitProperties()

    m_Style = Horizontal

End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    With PropBag
        m_Style = .ReadProperty("Style", 0)
        Line1.BorderColor = .ReadProperty("LineColor", &H99A8AC)
        Line2.BorderColor = .ReadProperty("ShadowColor", &HFFFFFF)
    End With

End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

    With PropBag
        Call .WriteProperty("Style", m_Style, 0)
        Call .WriteProperty("LineColor", Line1.BorderColor, &H99A8AC)
        Call .WriteProperty("ShadowColor", Line2.BorderColor, &HFFFFFF)
    End With

End Sub

Private Sub UserControl_Resize()

    If m_Style = Horizontal Then
        UserControl.Height = UserControl.ScaleY(2, vbPixels, vbTwips)
    Else
        UserControl.Width = UserControl.ScaleX(2, vbPixels, vbTwips)
    End If

    Call Draw3DLine

End Sub

Private Sub Draw3DLine()

    If m_Style = Horizontal Then
        With Line1
            .X1 = 0
            .Y1 = 0
            .X2 = UserControl.ScaleWidth
            .Y2 = 0
        End With

        With Line2
            .X1 = 0
            .Y1 = 1
            .X2 = UserControl.ScaleWidth
            .Y2 = 1
        End With
    Else
        With Line1
            .X1 = 0
            .Y1 = 0
            .X2 = 0
            .Y2 = UserControl.ScaleHeight
        End With

        With Line2
            .X1 = 1
            .Y1 = 0
            .X2 = 1
            .Y2 = UserControl.ScaleHeight
        End With
    End If

End Sub
